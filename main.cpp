#include <iostream>

using namespace std;

int main(int argc, char* argv[]){
    
    // Demo on void pointers

    int x = 44;

    void* p = &x;

    int* q = &x;

    cout << *(int*)p << endl;

	return 0;
}
